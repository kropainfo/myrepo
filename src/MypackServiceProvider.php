<?php

namespace Mydev\Mypack;

use Illuminate\Support\ServiceProvider;

class MypackServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        include __DIR__.'/routes.php';
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        // register our controller
        $this->app->make('Mydev\Mypack\Controllers\MypackController');
        $this->loadViewsFrom(__DIR__.'/views', 'mypack');
    }
}
