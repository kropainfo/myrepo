<?php

namespace Mydev\Mypack\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MypackController extends Controller
{
    public function add($a, $b){
        $result = $a + $b;
        return view('mypack::add', compact('result'));
    }

    public function subtract($a, $b){
        echo $a - $b;
    }
}
