<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('mypack', function(){
    echo 'Hello from the calculator package!';
});

Route::get('mypack/add/{a}/{b}', 'Mydev\Mypack\Controllers\MypackController@add');
Route::get('mypack/subtract/{a}/{b}', 'Mydev\Mypack\Controllers\MypackController@subtract');
